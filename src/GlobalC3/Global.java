/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GlobalC3;

/**
 *
 * @author 52667
 */
public class Global {
    int noBoleto;
    String nombreCliente;
    int añosCumplidos;
    String destino;
    int tipoViaje;
    float precio;

    public Global() {
        this.noBoleto = 0; 
        this.nombreCliente = "";
        this.añosCumplidos = 0; 
        this.destino = "";
        this.tipoViaje = 0;
        this.precio = 0.0f;
    }
    
    public Global(int noCliente, String nombreCliente, int añosCumplidos, String destino, int tipoViaje, float precio) {
        this.noBoleto = noCliente;
        this.nombreCliente = nombreCliente;
        this.añosCumplidos = añosCumplidos;
        this.destino = destino;
        this.tipoViaje = tipoViaje;
        this.precio = precio;
    }

    public int getNoCliente() {
        return noBoleto;
    }

    public void setNoCliente(int noCliente) {
        this.noBoleto = noCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public int getAñosCumplidos() {
        return añosCumplidos;
    }

    public void setAñosCumplidos(int añosCumplidos) {
        this.añosCumplidos = añosCumplidos;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public int getTipoViaje() {
        return tipoViaje;
    }

    public void setTipoViaje(int tipoViaje) {
        this.tipoViaje = tipoViaje;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
             
    
    public void Precio(float precio) {
    if (tipoViaje == 1) {
        this.precio = precio;
    } else if (tipoViaje == 2) {
        
        this.precio = precio * 1.8f;
        
        System.out.println("Tipo de viaje no valido.");
         }
    }
    
    
    
    public float calculaImpuesto(){
        float impuesto = precio * 1.16f;
        
        
        return impuesto;
    }

    public float calcularDescuento(){
        float descuento = 0.0f;
        
        if (añosCumplidos >= 60){
            descuento = precio * 0.5f;
        }
        return descuento;
    }

    
    
    public void Global(){
        float impuesto = calculaImpuesto();
        float descuento = calcularDescuento();
        float subtotal = precio - descuento - impuesto;
        float totalPagar = subtotal + impuesto + descuento;
        System.out.println("Numero de boleto :" + this.noBoleto);
        System.out.println("Nombre del cliente :" + this.nombreCliente);
        System.out.println("años cumplidos :" + this.añosCumplidos);
        System.out.println("Destino :" + this.destino);
        System.out.println("Tipo de viaje: " + this.tipoViaje); 
        System.out.println("Impuesto" + impuesto);
        System.out.println("Descuento" + descuento);
        System.out.println("SubTotal" + subtotal );
        System.out.println("Total a pagar :"+ totalPagar);
  }
}